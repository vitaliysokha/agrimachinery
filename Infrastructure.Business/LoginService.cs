﻿using System;
using System.Collections.Generic;
using Services.Interfaces;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Domain.Entities.Models;
using Domain.Entities.ViewModels;
using Microsoft.IdentityModel.Tokens;
using Infrastructure.Business.Auth;
using Domain.Entities.Communication;

namespace Infrastructure.Business
{
    public class LoginService: ILoginService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;


        public LoginService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }


        public TokenViewModel GetToken(ClaimsIdentity identity)
        {
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: DateTime.UtcNow,
                claims: identity.Claims,
                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var tokenResponse = new TokenViewModel
            {
                Token = encodedJwt,
                Roles = identity.Claims?.Where(p => p.Type == ClaimsIdentity.DefaultRoleClaimType).Select(x => x.Value)
                    .ToList(),
                UserId = identity.Name
            };

            return tokenResponse;
        }

        public async Task<Response<ClaimsIdentity>> GetIdentity(string login, string password)
        {
            Response<ClaimsIdentity> response = new Response<ClaimsIdentity>();

            var user = await GetUserByCreds(login, password);
            if (user == null)
            {
                response.Error = new Error(404, "Invalid username or password");
                return response;
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            List<Claim> claims = new List<Claim>();
            foreach (var role in userRoles)
            {
                var identityRole = await _roleManager.FindByNameAsync(role);
                var cl = await _roleManager.GetClaimsAsync(identityRole);
                claims.AddRange(cl);
            }
            var claimUserId = new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id);

            claims.Add(claimUserId);

            ClaimsIdentity claimsIdentity = new ClaimsIdentity
                (claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            response.Data = claimsIdentity;
            return response;
        }

        public async Task<User> GetUserByCreds(string login, string password)
        {

            //    var encrypted = TripleDESCryptHelper.Encript(password);
            var user = await _userManager.Users.FirstOrDefaultAsync(
                p => p.Email == login);
            var verify = await _userManager.CheckPasswordAsync(user, password);
            if (!verify)
            {
                return null;
            }
            return user;
        }
    }
}
