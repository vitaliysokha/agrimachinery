﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities.Communication;
using Domain.Entities.Enums;
using Domain.Entities.Models;
using Domain.Entities.ViewModels;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Services.Interfaces;

namespace Infrastructure.Business
{
    public class PublicationService: IPublicationService
    {
        protected readonly IUnitOfWork _unitOfWork;

        public PublicationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddLike(string publicationId)
        {
            var publication = await _unitOfWork.Publication.FindOneAsync(x => x.Id.Equals(publicationId));
            publication.Likes++;
            await _unitOfWork.Publication.Update(publication);
        }

        public async Task<Response<Publication>> AddPublication(PublicationViewModel model)
        {
            Response<Publication> response = new Response<Publication>();

            List<Image> images = new List<Image>();

            //foreach (var image in model.Images)
            //{
            //    var r = new Image { ImagePath = image };
            //    images.Add(r);
            //}
        
            model.Images.ForEach(imagePath => images.Add(new Image{ImagePath = imagePath}));
         
            var publication = new Publication
            {
                Title = model.Title,
                Description = model.Description,
                Category = model.Category,
                Date = DateTime.UtcNow,
                Images = images,
                Likes = 0,
                Views = 0
            };

            await _unitOfWork.Publication.InsertOneAsync(publication);
            response.Data = publication;
            return response;
        }

        public async Task DeletePublication(string publicationId)
        {
           await _unitOfWork.Publication.DeleteOneAsync(publicationId);
        }

        public async Task<Response<Publication>> GetPublication(string publicationId)
        {
            Response<Publication> response = new Response<Publication>();

            var publication = await _unitOfWork.Publication.GetOneWithInclude(predicate: x=>x.Id.Equals(publicationId), includeProperties: x=>x.Images);
            response.Data = publication;
            return response;
        }

        public async Task<Response<List<Publication>>> GetPublications(int limit = 50, int skip = 0)
        {
            Response<List<Publication>> response = new Response<List<Publication>>();
            var publications = await _unitOfWork.Publication.GetManyWithInclude(limit, skip, x=>x.Images);

            response.Data = publications;
            return response;
        }

        public async Task<Response<List<Publication>>> GetPublicationsByCategory(CategoriesEnum category,
            int limit = 50, int skip = 0)
        {
            Response<List<Publication>> response = new Response<List<Publication>>();
            var publications =
                await _unitOfWork.Publication.GetManyWithInclude(x => x.Category.Equals(category), limit, skip, x=>x.Images);
            response.Data = publications;
            return response;
        }

        public async Task UpdatePublication(string publicationId, PublicationViewModel model)
        {

            List<Image> images = new List<Image>();
            model.Images.ForEach(imagePath => images.Add(new Image { ImagePath = imagePath }));

            var publication = await _unitOfWork.Publication.FindOneAsync(publicationId);
            publication.Title = model.Title;
            publication.Description = model.Description;
            publication.Category = model.Category;
            publication.Images = images;
           

            await _unitOfWork.Publication.Update(publication);
        }

        public async Task UpdateViews(string publicationId)
        {
            var publication = await _unitOfWork.Publication.FindOneAsync(x => x.Id.Equals(publicationId));
            publication.Views++;
            await _unitOfWork.Publication.Update(publication);
        }
    }
    
}
