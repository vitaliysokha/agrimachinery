﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Infrastructure.Business.Auth
{
    public class AuthOptions
    {
        public const string ISSUER = "agri_machinery_web_api";
        public const string AUDIENCE = "agri_machinery_web_api";
        const string KEY = "security_80_lvl.ty_ne_proydesh";
        public const int LIFETIME = 60 * 24;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
