﻿using System;
using System.Threading.Tasks;
using Domain.Entities.Communication;
using Domain.Entities.Enums;
using Domain.Entities.Models;
using Domain.Entities.ViewModels;
using Microsoft.AspNetCore.Identity;
using Services.Interfaces;

namespace Infrastructure.Business
{
    public class AccountService : IAccountService
    {
        
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AccountService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<Response<OperationResult>> Register(RegisterViewModel model)
        {
            var response = new Response<OperationResult>()
            {
                Data = OperationResult.Failed
            };

            var user = new User
            {
                Email = model.Email,
                UserName = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                Phone = model.Phone,
            };
            var userExixt = await _userManager.FindByNameAsync(model.Email);
            if (userExixt != null)
            {
                response.Error = new Error(400, "This user is already registered!");
                return response;
            }

            var creationUser = await _userManager.CreateAsync(user, model.Password);

            if (creationUser.Succeeded)
            {

                var addingToRole = await _userManager.AddToRoleAsync(user, Roles.user.ToString());
                if (addingToRole.Succeeded)
                {
                    response.Data = OperationResult.Success;
                    return response;
                }

                response.Error = new Error(404, "Error while adding role to user!");
                return response;
            }

            foreach (var error in creationUser.Errors)
            {
                response.Error = new Error(404, error.Description);
            }

            return response;
        }
    }
    
}