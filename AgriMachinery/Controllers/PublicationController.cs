﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.Enums;
using Domain.Entities.Models;
using Domain.Entities.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;

namespace AgriMachinery.Controllers
{
    [Produces("application/json")]
    [Route("api/Publication")]
    public class PublicationController : Controller
    {
        private readonly IPublicationService _publicationService;

        public PublicationController(IPublicationService publicationService)
        {
            _publicationService = publicationService;
        }
        // POST: api/Categories
        [HttpPost]
        public async Task<IActionResult> PostPublication([FromBody] PublicationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _publicationService.AddPublication(model);
           
            return Ok(result.Data);
        }

        [HttpGet]
        public async Task<IActionResult> GetPublications(int limit=50, int skip = 0)
        {
            var result = await _publicationService.GetPublications(limit, skip);
            if (!result.Data.Any())
            {
                return NoContent();
            };
            return Ok(result.Data);
        }
        [HttpGet("{publicationId}")]
        public async Task<IActionResult> GetPublication(string publicationId)
        {
            var result = await _publicationService.GetPublication(publicationId);
            if (result.Data==null)
            {
                return NoContent();
            };
            return Ok(result.Data);
        }

        [HttpGet]
        [Route("ByCategory")]
        public async Task<IActionResult> GetPublicationsByCategory(CategoriesEnum category, int limit = 50, int skip = 0)
        {
            var result = await _publicationService.GetPublicationsByCategory(category, limit, skip);
            if (!result.Data.Any())
            {
                return NoContent();
            };
            return Ok(result.Data);
        }

        [HttpPut("{publicationId}")]
        public async Task<IActionResult> UpdatePublication(string publicationId, PublicationViewModel model)
        {
            await _publicationService.UpdatePublication(publicationId, model);
            return Ok();
        }

        [HttpPut("addLike/{publicationId}")]
        public async Task<IActionResult> Like(string publicationId)
        {
            await _publicationService.AddLike(publicationId);
            return Ok();
        }

        [HttpPut("addView/{publicationId}")]
        public async Task<IActionResult> UpdateView(string publicationId)
        {
            await _publicationService.UpdateViews(publicationId);
            return Ok();
        }

        // DELETE: api/Publication/5
        [HttpDelete("{publicationId}")]
        public async Task<IActionResult> DeletePublication( string publicationId)
        {
            await _publicationService.DeletePublication(publicationId);

            return Ok();
        }

    }
}