﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain.Entities.Enums;
using Domain.Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace AgriMachinery.Utils
{
    public static class IdentityInitializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            string adminEmail = "admin@gmail.com";
            string password = "_Aa123456";
            if (await roleManager.FindByNameAsync("admin") == null)
            {
                var role = new IdentityRole(Roles.admin.ToString());
                await roleManager.CreateAsync(role);
                await roleManager.AddClaimAsync(role, new Claim(ClaimsIdentity.DefaultRoleClaimType, Roles.admin.ToString()));
            }
            if (await roleManager.FindByNameAsync("user") == null)
            {
                var role = new IdentityRole(Roles.user.ToString());
                await roleManager.CreateAsync(role);
                await roleManager.AddClaimAsync(role, new Claim(ClaimsIdentity.DefaultRoleClaimType, Roles.user.ToString()));
            }
            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                User admin = new User { Email = adminEmail, UserName = adminEmail };
                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin");
                }
            }
        }
        //public static void Initialize(IServiceProvider services)
        //{

        //    using (var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
        //    {
        //        var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
        //        var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
        //        var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
        //        if (userManager.FindByNameAsync("Admin").)

        //            if (roleManager.FindByNameAsync(Roles.Admin.ToString()).Result == null)
        //            {
        //                var role = new IdentityRole();
        //                role.Name = Roles.Admin.ToString();
        //                roleManager.CreateAsync(role).Wait();
        //                roleManager.AddClaimAsync(role,
        //                    new Claim(ClaimsIdentity.DefaultRoleClaimType, Roles.Admin.ToString())).Wait();
        //            }
        //        if (roleManager.FindByNameAsync(Roles.User.ToString()).Result == null)
        //        {
        //            var role = new IdentityRole();
        //            role.Name = Roles.User.ToString();
        //            roleManager.CreateAsync(role).Wait();
        //            roleManager.AddClaimAsync(role,
        //                new Claim(ClaimsIdentity.DefaultRoleClaimType, Roles.User.ToString())).Wait();
        //        }
        //    }
        //}
    }
}