﻿using Microsoft.AspNetCore.Identity;

namespace Domain.Entities.Models
{
    public class User: IdentityUser
    {
        public string Phone { get; set; }
    }
}