﻿using Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models
{
    public class Publication
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public CategoriesEnum Category { get; set; }
        public int Likes { get; set; }
        public int Views { get; set; }
        public DateTime Date { get; set; }
        public virtual List<Image> Images { get; set; }
    }
}