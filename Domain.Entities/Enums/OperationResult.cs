﻿namespace Domain.Entities.Enums
{
    public enum OperationResult
    {
        Success,
        Failed
    }
}