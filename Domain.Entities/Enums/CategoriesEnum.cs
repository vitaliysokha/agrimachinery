﻿namespace Domain.Entities.Enums
{
    public enum CategoriesEnum
    {
        Combine,
        Tractor,
        Press,
        Parts,
        Other
    }
}