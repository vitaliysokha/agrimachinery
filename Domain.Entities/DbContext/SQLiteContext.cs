﻿using System.Security.Cryptography.X509Certificates;
using Domain.Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Domain.Entities.DbContext
{
    public class SQLiteContext: IdentityDbContext<User>
    {
        public SQLiteContext(DbContextOptions options) : base(options)
        {
            
        }

        public virtual DbSet<Publication> Publications { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<IdentityUser>().ToTable("Users");
            builder.Entity<IdentityUser>().Ignore(x => x.AccessFailedCount);
            builder.Entity<IdentityUser>().Ignore(x => x.LockoutEnabled);
            builder.Entity<IdentityUser>().Ignore(x => x.ConcurrencyStamp);
            builder.Entity<IdentityUser>().Ignore(x => x.EmailConfirmed);
            builder.Entity<IdentityUser>().Ignore(x => x.LockoutEnd);
            builder.Entity<IdentityUser>().Ignore(x => x.PhoneNumberConfirmed);
            builder.Entity<IdentityUser>().Ignore(x => x.TwoFactorEnabled);


            builder.Entity<Publication>().HasMany(x => x.Images).WithOne(x=>x.Publication).HasForeignKey(x=>x.PublicationId)
             .OnDelete(DeleteBehavior.Cascade);





        }
    }
}