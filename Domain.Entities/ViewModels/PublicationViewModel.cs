﻿using Domain.Entities.Enums;
using Domain.Entities.Models;
using System;
using System.Collections.Generic;

namespace Domain.Entities.ViewModels
{
    public class PublicationViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public CategoriesEnum Category { get; set; }
        public List<string> Images { get; set; }
    }
}