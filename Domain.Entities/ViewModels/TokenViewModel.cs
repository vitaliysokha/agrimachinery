﻿using System.Collections.Generic;

namespace Domain.Entities.ViewModels
{
    public class TokenViewModel
    {
        public string Token { get; set; }
        public List<string> Roles { get; set; }
        public string UserId { get; set; }
    }
}