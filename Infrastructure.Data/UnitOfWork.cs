﻿using System.Threading.Tasks;
using Domain.Entities.DbContext;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Infrastructure.Data.Repositories;

namespace Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private IPublicationRepository _publication;
        private readonly SQLiteContext _dataContext;

        public UnitOfWork(SQLiteContext dataContext)
        {
            _dataContext = dataContext;
        }


        public IPublicationRepository Publication
        {
            get { return _publication = _publication ?? new PublicationRepository(_dataContext); }
        }

        public void Dispose()
        {
            _dataContext.Dispose();
        }

        public async Task SaveAsync()
        {
            await _dataContext.SaveChangesAsync();
        }
    }
}