﻿using Domain.Entities.DbContext;
using Domain.Entities.Models;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Data.Repositories
{
    public class PublicationRepository : Repository<Publication>, IPublicationRepository
    {
        public PublicationRepository(SQLiteContext context) : base(context)
        {

        }
    }
}