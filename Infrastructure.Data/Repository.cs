﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.Entities.DbContext;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected SQLiteContext _dataContext;
        protected DbSet<TEntity> _dbSet;

        public Repository(SQLiteContext context)
        {
            _dataContext = context;
            _dbSet = context.Set<TEntity>();
        }

        public DbSet<TEntity> DbSet
        {
            get
            {
                if (_dbSet == null)
                {
                    _dbSet = _dataContext.Set<TEntity>();
                }
                return _dbSet;
            }
        }
   

        #region Find

        public async Task<TEntity> FindOneAsync(string id)
        {
            return  await _dbSet.FindAsync(id);
        }
        public async Task<TEntity> FindOneAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.AsNoTracking().FirstOrDefaultAsync(predicate);
        }
         public async Task<TEntity> GetOneWithInclude(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var include = Include(includeProperties);
            return await include.Where(predicate).FirstOrDefaultAsync();
        }

        public async Task<List<TEntity>> GetAllAsync(int limit = 50, int skip = 0)
        {
            return await _dbSet.AsNoTracking().Take(limit).Skip(skip).ToListAsync();
        }

        public async Task<List<TEntity>> GetAllWithOrderDescAsync(Expression<Func<TEntity, object>> predicate, int limit = 50, int skip = 0)
        {
            return await _dbSet.AsNoTracking().OrderByDescending(predicate).Take(limit).Skip(skip).ToListAsync();
        }

        public async Task<List<TEntity>> GetAllWithOrderAscAsync(Expression<Func<TEntity, object>> predicate, int limit = 50, int skip = 0)
        {
            return await _dbSet.AsNoTracking().OrderBy(predicate).Take(limit).Skip(skip).ToListAsync();
        }

        public async Task<List<TEntity>> FindManyAsync(Expression<Func<TEntity, bool>> predicate, int limit = 50, int skip = 0)
        {
            return await _dbSet.AsNoTracking().Where(predicate).Take(limit).Skip(skip).ToListAsync();
        }

        public async Task<List<TEntity>> FindManyAsyncOrderByDesc(Expression<Func<TEntity, bool>> finderExpression, Expression<Func<TEntity, object>> sorttedExpression, int limit = 50, int skip = 0)
        {
            return await _dbSet.AsNoTracking().Where(finderExpression).OrderByDescending(sorttedExpression).Take(limit)
                .Skip(skip).ToListAsync();
        }

        public async Task<List<TEntity>> FindManyAsyncOrderByAsc(Expression<Func<TEntity, bool>> finderExpression, Expression<Func<TEntity, object>> sorttedExpression, int limit = 50, int skip = 0)
        {
            return await _dbSet.AsNoTracking().Where(finderExpression).OrderBy(sorttedExpression).Take(limit)
                .Skip(skip).ToListAsync();
        }

        public async Task<List<TEntity>> GetManyWithInclude(int limit=50, int skip = 0, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var include = Include(includeProperties).Take(limit).Skip(skip);
            return await include.ToListAsync();
        }

        public async Task<List<TEntity>> GetManyWithInclude(Expression<Func<TEntity, bool>> predicate, int limit = 50, int skip = 0, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var include = Include(includeProperties).Take(limit).Skip(skip);
            return await include.Where(predicate).ToListAsync();
        }


        private IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        #endregion

        #region Insert 

        public async Task InsertOneAsync(TEntity entity)
        {
             await _dbSet.AddAsync(entity);
             await _dataContext.SaveChangesAsync();
        }

        public async Task InsertManyAsync(List<TEntity> entities)
        {
            await _dbSet.AddRangeAsync(entities);
            await _dataContext.SaveChangesAsync();
        }

        #endregion

        #region Delete

        public async Task DeleteOneAsync(string id)
        {
            var item = await _dbSet.FindAsync(id);
            if (item == null)
            {
                throw new Exception("Item not found");
            }
            
            try
            {
                _dbSet.Remove(item);
                await _dataContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task DeleteManyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var item = await _dbSet.AsNoTracking().Where(predicate).ToListAsync();
            if (!item.Any())
            {
                throw new Exception("Items not found");
            }

            try
            {
                _dbSet.RemoveRange(item);
                await _dataContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }

        #endregion

        #region Update

        public async Task Update(TEntity item)
        {
            try
            {
                _dataContext.Entry(item).State = EntityState.Modified;
                await _dataContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
          
        }


        #endregion
    }
}
