﻿using Domain.Entities.Models;

namespace Domain.Interfaces.Repositories
{
    public interface IPublicationRepository: IRepository<Publication>
    {
        
    }
}