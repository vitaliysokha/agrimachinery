﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace Domain.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> FindOneAsync(string id);
        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindOneAsync(int id);

        Task<List<TEntity>> GetAllAsync(int limit = 50, int skip = 0);
        Task<List<TEntity>> GetAllWithOrderDescAsync(Expression<Func<TEntity, object>> predicate, int limit = 50, int skip = 0);
        Task<List<TEntity>> GetAllWithOrderAscAsync(Expression<Func<TEntity, object>> predicate, int limit = 50, int skip = 0);
        Task<List<TEntity>> GetManyWithInclude(int limit = 50, int skip = 0, params Expression<Func<TEntity, object>>[] includeProperties);
        Task<List<TEntity>> GetManyWithInclude(Expression<Func<TEntity, bool>> predicate, int limit = 50, int skip = 0, params Expression<Func<TEntity, object>>[] includeProperties);

        Task<TEntity> GetOneWithInclude(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties);

        Task<List<TEntity>> FindManyAsync(Expression<Func<TEntity, bool>> predicate, int limit = 50, int skip = 0);
        Task<List<TEntity>> FindManyAsyncOrderByDesc(Expression<Func<TEntity, bool>> finderExpression, Expression<Func<TEntity, object>> sorttedExpression, int limit = 50, int skip = 0);
        Task<List<TEntity>> FindManyAsyncOrderByAsc(Expression<Func<TEntity, bool>> finderExpression, Expression<Func<TEntity, object>> sorttedExpression, int limit = 50, int skip = 0);

        Task InsertOneAsync(TEntity entity);
        Task InsertManyAsync(List<TEntity> entities);

        Task DeleteOneAsync(string id);
        Task DeleteManyAsync(Expression<Func<TEntity, bool>> predicate);

        Task Update(TEntity item);
    }
}