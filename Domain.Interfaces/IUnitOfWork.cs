﻿using System;
using System.Threading.Tasks;
using Domain.Interfaces.Repositories;

namespace Domain.Interfaces
{
    public interface IUnitOfWork:IDisposable
    {
        IPublicationRepository Publication { get;}
        Task SaveAsync();
    }
}