﻿using Domain.Entities.Models;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain.Entities.ViewModels;
using Domain.Entities.Communication;

namespace Services.Interfaces
{
    public interface ILoginService
    {
        Task<User> GetUserByCreds(string login, string password);

        Task<Response<ClaimsIdentity>> GetIdentity(string login, string password);

        TokenViewModel GetToken(ClaimsIdentity identity);
    }
}