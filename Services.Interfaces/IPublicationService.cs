﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities.Communication;
using Domain.Entities.Enums;
using Domain.Entities.Models;
using Domain.Entities.ViewModels;

namespace Services.Interfaces
{
    public interface IPublicationService
    {
        Task<Response<Publication>> AddPublication(PublicationViewModel model);
        Task<Response<List<Publication>>> GetPublications(int limit = 50, int skip = 0);
        Task<Response<List<Publication>>> GetPublicationsByCategory(CategoriesEnum category, int limit = 50, int skip = 0);
        Task<Response<Publication>> GetPublication(string id);
        Task AddLike(string publicationId);
        Task UpdateViews(string publicationId);
        Task UpdatePublication(string publicationId, PublicationViewModel publication);
        Task DeletePublication(string publicationId);

    }
}