﻿using Domain.Entities.Enums;
using System.Threading.Tasks;
using Domain.Entities.ViewModels;
using Domain.Entities.Communication;

namespace Services.Interfaces
{
    public interface IAccountService
    {
        Task<Response<OperationResult>> Register(RegisterViewModel model);
    }
}